package ui

//go:generate npm install
//go:generate env PROD=1 ./node_modules/.bin/webpack
//go:generate go run generator/main.go

import (
	"html/template"
	"io"
)

type Params struct {
	BaseURL string
	*HeadParams
	*ErrorParams
	*ShortenedParams
}

type HeadParams struct {
	CSSShaAlgo template.HTML
	CSSSha     template.HTML
	CSSShort   string
}

type ErrorParams struct {
	Code int
	Msg  string
}

type ShortenedParams struct {
	Original  string
	Shortened string
}

const head = `{{ define "head" }}
<head>
    <title>shottino: shorten urls</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ .BaseURL }}/static/{{ .HeadParams.CSSShort }}" integrity="{{ .HeadParams.CSSShaAlgo }}-{{ .HeadParams.CSSSha }}">
</head>
{{ end }}
`

const bodyStart = `{{ define "bodyStart" }}
<body class="text-center">
    <div class="container">
        <div class="w-100 h-100 align-items-center justify-items-center">
{{ end }}
`

const bodyClose = `{{ define "bodyClose" }}
        </div>
    </div>
</body>
{{ end }}
`

const home = `
{{ define "home" }}
<!DOCTYPE html>
<html>
{{ template "head" . }}
{{ template "bodyStart" }}
    <form action="{{ .BaseURL }}/api/shorten" method="post">
    <div class="form-group">
    <input class="form-control form-control-lg" type="text" id="address" name="address" placeholder="Address" />
    <small id="address-help" class="form-text text-muted">Enter the address to be shortened</small>
    </div>
    <button class="btn btn-primary align-center" type="submit">Shorten</button>
    </form>
{{ template "bodyClose" }}
</html>
{{ end }}
`

const errPage = `
{{ define "errPage" }}
<!DOCTYPE html>
<html>
{{ template "head" . }}
{{ template "bodyStart" }}
<div class="jumbotron">
    <h1 class="display-4">Error {{ .Code }}</h1>
    <p class="lead">{{ .Msg }}</p>
    <hr class="my-4">
    <p>Head back home:</p>
    <a class="btn btn-primary btn-lg" href="{{ .BaseURL }}/" role="button">Home</a>
</div>
{{ template "bodyClose" }}
</html>
{{ end }}
`

const shortened = `
{{ define "shortened" }}
<!DOCTYPE html>
<html>
{{ template "head" . }}
{{ template "bodyStart" }}
    <div>Original: <a href="{{ .Original }}">{{ .Original }}</a></div>
    <br/>
    <div>Shortened: <a href="{{ .Shortened }}">{{ .Shortened }}</a></div>
{{ template "bodyClose" }}
</html>
{{ end }}
`

var (
	tmpl = template.New("tmpl")
	_    = template.Must(tmpl.Parse(head))
	_    = template.Must(tmpl.Parse(bodyStart))
	_    = template.Must(tmpl.Parse(bodyClose))
	_    = template.Must(tmpl.Parse(home))
	_    = template.Must(tmpl.Parse(errPage))
	_    = template.Must(tmpl.Parse(shortened))
	css  = template.Must(template.New("css").Parse(CSS))
)

var CSSInfo = &HeadParams{
	CSSShaAlgo: template.HTML(CSSAlgo),
	CSSSha:     template.HTML(CSSSha),
	CSSShort:   CSSShort,
}

func RenderHome(w io.Writer, baseURL string) error {
	return tmpl.ExecuteTemplate(w, "home", &Params{
		BaseURL:    baseURL,
		HeadParams: CSSInfo,
	})
}

func RenderErrPage(w io.Writer, baseURL string, params *ErrorParams) error {
	return tmpl.ExecuteTemplate(w, "errPage", &Params{
		BaseURL:     baseURL,
		HeadParams:  CSSInfo,
		ErrorParams: params,
	})
}

func RenderShortened(w io.Writer, baseURL string, params *ShortenedParams) error {
	return tmpl.ExecuteTemplate(w, "shortened", &Params{
		BaseURL:         baseURL,
		HeadParams:      CSSInfo,
		ShortenedParams: params,
	})
}

func RenderCSS(w io.Writer) error {
	return css.ExecuteTemplate(w, "css", nil)
}
