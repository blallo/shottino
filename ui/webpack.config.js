const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PROD = JSON.parse(process.env.PROD || '0');

module.exports = {
    mode: PROD ? 'production' : 'development',
    output: {
        publicPath: './',
        path: __dirname + '/assets',
        filename: 'static/[name].[contenthash].js',
        assetModuleFilename: 'static/[name][ext]',
        clean: true,
        crossOriginLoading: 'anonymous',
    },
    plugins: [new MiniCssExtractPlugin({
        filename: 'static/[name].[contenthash].css',
    })],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
        ],
    },
};
