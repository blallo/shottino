package main

import (
	"bytes"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"
)

const tmpl = `// Code generated - DO NOT EDIT.

package ui

const (
    CSSShort = {{ .Enclose .Short }}
    CSSAlgo = {{ .Enclose "sha384" }}
    CSSSha = {{ .Enclose .Sum }}
)

const CSS = {{ .Enclose .Content }}
`

var gen = template.Must(template.New("generated.go").Parse(tmpl))

type InputData struct {
	Short   string
	Sum     string
	Content string
}

func (i *InputData) Enclose(in string) string {
	return fmt.Sprintf("`%s`", in)
}

func cleanContent(content []byte) []byte {
	return bytes.TrimRight(content, " \t\n")
}

func getSha384(content []byte) string {
	var sha []byte

	for _, b := range sha512.Sum384(content) {
		sha = append(sha, b)
	}

	buf := bytes.NewBuffer(nil)
	enc := base64.NewEncoder(base64.StdEncoding, buf)
	enc.Write(sha)
	enc.Close()

	return string(buf.Bytes())
}

func main() {
	cssPath, err := filepath.Glob("./assets/static/main.*.css")
	if err != nil {
		panic(err)
	}
	if len(cssPath) != 1 {
		panic(fmt.Sprintf("unexpected file(s): %s", cssPath))
	}

	cssContentBytes, err := ioutil.ReadFile(cssPath[0])
	if err != nil {
		panic(err)
	}

	cssContent := cleanContent(cssContentBytes)

	sha := getSha384(cssContent)

	w, err := os.OpenFile("./generated.go", os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}

	if err = gen.ExecuteTemplate(w, "generated.go", &InputData{
		Short:   filepath.Base(cssPath[0]),
		Sum:     sha,
		Content: string(cssContent),
	}); err != nil {
		panic(err)
	}
}
