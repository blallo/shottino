package api

import (
	"net/http"

	"git.autistici.org/blallo/shottino/ui"
)

type log struct {
	k string
	v interface{}
}

func logData(base map[string]interface{}, other ...log) map[string]interface{} {
	result := make(map[string]interface{})

	for k, v := range base {
		result[k] = v
	}

	for _, l := range other {
		result[l.k] = l.v
	}

	return result
}

func htmlError(w http.ResponseWriter, msg, baseURL string, code int) {
	w.Header().Set("Content-Type", "text/html")

	w.WriteHeader(code)
	ui.RenderErrPage(w, baseURL, &ui.ErrorParams{
		Code: code,
		Msg:  msg,
	})
}
