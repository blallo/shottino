package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"

	"git.sr.ht/~blallo/logz/interface"

	"git.autistici.org/blallo/shottino"
	"git.autistici.org/blallo/shottino/ui"
)

var logMapShortener = map[string]interface{}{"handler": "Shortener"}

type ShortenJSONBody struct {
	Address string `json:"address"`
}

type Shortener struct {
	shottino.Store
	logz.Logger
	shottino.Generator
	BaseURL    string
	ValidURLRe *regexp.Regexp
}

func (h *Shortener) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		msg := logData(logMapShortener, log{"msg", "method not allowed"})
		h.Logger.Debug(msg)
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	format := r.Form.Get("format")
	if format == "json" || r.Header.Get("Content-Type") == "application/json" {
		h.parseJSON(w, r)
	} else {
		h.parseForm(w, r)
	}

}

func (h *Shortener) parseJSON(w http.ResponseWriter, r *http.Request) {
	var body ShortenJSONBody

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		msg := logData(logMapShortener,
			log{"msg", "malformed request: could not parse json"},
			log{"err", err},
		)
		h.Logger.Info(msg)
		http.Error(w, "Bad request: could not parse json", http.StatusBadRequest)
		return
	}

	h.store(w, r, body.Address, true)
}

func (h *Shortener) parseForm(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		msg := logData(logMapShortener,
			log{"msg", "malformed request: not a form"},
			log{"err", err},
		)
		h.Logger.Info(msg)
		htmlError(w, "Bad request: not a form", h.BaseURL, http.StatusBadRequest)
		return
	}

	addrStr := r.PostForm.Get("address")
	if addrStr == "" {
		h.Logger.Info(logData(logMapShortener, log{"msg", "malformed request: missing addres"}))
		htmlError(w, "Bad request: missing address", h.BaseURL, http.StatusBadRequest)
		return
	}

	h.store(w, r, addrStr, false)
}

func (h *Shortener) store(w http.ResponseWriter, r *http.Request, addrStr string, apiCall bool) {
	address, err := h.parseURL(addrStr)
	if err != nil {
		var msg map[string]interface{}
		var code int

		if _, ok := err.(*errInvalidURL); ok {
			msg = logData(logMapShortener,
				log{"msg", "invalid: address not acceptable"},
				log{"addr", addrStr},
				log{"err", err.Error()},
			)
			code = http.StatusForbidden
		} else {
			msg = logData(logMapShortener,
				log{"msg", "maformed request: address not parsable"},
				log{"addr", addrStr},
				log{"err", err},
			)
			code = http.StatusBadRequest
		}
		h.Logger.Info(msg)
		if apiCall {
			http.Error(w, msg["msg"].(string), code)
		} else {
			htmlError(w, msg["msg"].(string), h.BaseURL, code)
		}
		return
	}

	var short string

setLoop:
	for {
		short = h.Generator.Do()

		err = h.Store.Set(r.Context(), short, address)
		if err != nil {
			if e, ok := err.(*shottino.ErrAlreadyThere); !ok {
				continue
			} else {
				short = e.Short
				msg := logData(logMapShortener,
					log{"msg", "address already shortened"},
					log{"addr", addrStr},
					log{"short", short},
				)
				h.Logger.Warn(msg)
			}
		} else {
			h.Logger.Info(logData(
				logMapShortener,
				log{"msg", "created"},
				log{"short", short},
			))
		}
		break setLoop
	}

	if apiCall {
		h.replyApi(w, r, short, addrStr)
		return
	}
	h.show(w, r, short, addrStr)
}

func (h *Shortener) replyApi(w http.ResponseWriter, r *http.Request, short, orig string) {
	reply := make(map[string]string)
	reply["short"] = fmt.Sprintf("%s/s/%s", h.BaseURL, short)
	reply["original"] = orig

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(reply); err != nil {
		h.Logger.Warn(logData(
			logMapShortener,
			log{"msg", "failed to reply"},
			log{"err", err.Error()},
		))
	}
}

func (h *Shortener) show(w http.ResponseWriter, r *http.Request, short, orig string) {
	shortAddr := fmt.Sprintf("%s/s/%s", h.BaseURL, short)

	w.Header().Set("Content-Type", "text/html")

	if err := ui.RenderShortened(w, h.BaseURL, &ui.ShortenedParams{
		Original:  orig,
		Shortened: shortAddr,
	}); err != nil {
		h.Logger.Warn(logData(
			logMapShortener,
			log{"msg", "failed to reply"},
			log{"err", err.Error()},
		))
	}
}

func (h *Shortener) parseURL(addrStr string) (*url.URL, error) {
	if !h.ValidURLRe.MatchString(addrStr) {
		return nil, &errInvalidURL{url: addrStr}
	}

	return url.Parse(addrStr)
}

type errInvalidURL struct {
	url string
}

func (e *errInvalidURL) Error() string {
	return e.url
}
