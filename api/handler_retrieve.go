package api

import (
	"net/http"
	"regexp"

	"git.sr.ht/~blallo/logz/interface"

	"git.autistici.org/blallo/shottino"
)

var logMapRetriever = map[string]interface{}{"handler": "Retriever"}

type retrieverParams struct {
	logz.Logger
	shottino.Store
	re string
}

func newRetriever(p *retrieverParams) *Retriever {
	re := regexp.MustCompile(p.re)
	p.Logger.Debug(logData(logMapRetriever, log{"msg", "regexp generated"}, log{"regexp", re.String()}))

	return &Retriever{
		Logger:  p.Logger,
		Store:   p.Store,
		shortRe: re,
	}
}

type Retriever struct {
	logz.Logger
	shottino.Store
	shortRe *regexp.Regexp
}

func (h *Retriever) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		msg := logData(logMapRetriever, log{"msg", "method not allowed"})
		h.Logger.Debug(msg)
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	matches := h.shortRe.FindStringSubmatch(r.URL.Path)
	h.Logger.Debug(logData(logMapRetriever, log{"msg", "match result"}, log{"match", matches}))
	if len(matches) != 2 {
		msg := logData(logMapRetriever,
			log{"msg", "bad request: unexpected url"},
			log{"path", r.URL.Path},
		)
		h.Logger.Info(msg)
		http.Error(w, "Bad request: malformed path", http.StatusBadRequest)
		return
	}
	short := matches[1]

	dest, err := h.Store.Get(r.Context(), short)
	if err != nil {
		msg := logData(logMapRetriever,
			log{"msg", "internal error: failed to get from store"},
			log{"err", err},
		)
		h.Logger.Err(msg)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	if dest == nil {
		msg := logData(logMapRetriever,
			log{"msg", "not found"},
			log{"short", short},
		)
		h.Logger.Info(msg)
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	h.Logger.Debug(logData(logMapRetriever,
		log{"msg", "found destination"},
		log{"short", short},
		log{"dest", dest.String()},
	))

	http.Redirect(w, r, dest.String(), http.StatusFound)
}
