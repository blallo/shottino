//go:build !integration

package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"git.sr.ht/~blallo/logz/interface"
	"git.sr.ht/~blallo/logz/jsonlogger"
)

const (
	short = "short123"
	dest  = "https://example.com/the/long/address"
)

func TestRetriever(t *testing.T) {
	handler := newRetriever(&retrieverParams{
		Store: &mockStore{
			expected: map[string]string{
				short: dest,
			},
		},
		Logger: logger.NewJSONLogger().SetLevel(logz.LogDebug),
		re:     "^/s/([a-zA-Z0-9_-]+)$",
	})

	req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/s/%s", short), nil)
	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != http.StatusFound {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}
	if loc := resp.Header.Get("Location"); loc != dest {
		t.Fatalf("Unexpected location: %s", loc)
	}

	req = httptest.NewRequest(http.MethodGet, "/s/notThere", nil)
	w = httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp = w.Result()

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}

	req = httptest.NewRequest(http.MethodGet, "/s/b/c", nil)
	w = httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp = w.Result()

	if resp.StatusCode != http.StatusBadRequest {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}
}
