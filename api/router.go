package api

import (
	"fmt"
	"io"
	"net/http"
	"regexp"

	"git.sr.ht/~blallo/logz/interface"

	"git.autistici.org/blallo/shottino"
	"git.autistici.org/blallo/shottino/generator"
	"git.autistici.org/blallo/shottino/ui"
)

type route struct {
	*regexp.Regexp
	http.Handler
}

func newRoute(path string, handler http.Handler) *route {
	re := regexp.MustCompile(fmt.Sprintf("^%s$", path))

	return &route{
		Regexp:  re,
		Handler: handler,
	}
}

type Router struct {
	logz.Logger
	shottino.Store
	shottino.Generator

	routes []*route
}

func (h *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, p := range h.routes {
		if p.Regexp.MatchString(r.URL.Path) {
			p.Handler.ServeHTTP(w, r)
			return
		}
	}

	h.Logger.Info(map[string]interface{}{
		"msg":     "No route found",
		"path":    r.URL.Path,
		"handler": "Router",
	})
	http.Error(w, "No route found", http.StatusNotFound)
}

type RouterParams struct {
	logz.Logger
	shottino.Store
	ShortLen   int
	BaseURL    string
	ValidURLRe *regexp.Regexp
}

func NewRouter(p *RouterParams) *Router {
	generator := generator.NewGenerator(p.ShortLen)

	routes := []*route{
		newRoute("(/)?", newHomeHandler(&homeHandlerParams{
			Logger:  p.Logger,
			baseURL: p.BaseURL,
		})),
		newRoute(fmt.Sprintf("/s/[%s]+", generator.Charset()), newRetriever(&retrieverParams{
			Logger: p.Logger,
			Store:  p.Store,
			re:     fmt.Sprintf("^/s/([%s]+)$", generator.Charset()),
		})),
		newRoute(`/api/shorten(\?format=[\w]+)?`, &Shortener{
			Logger:     p.Logger,
			Store:      p.Store,
			Generator:  generator,
			BaseURL:    p.BaseURL,
			ValidURLRe: p.ValidURLRe,
		}),
		newRoute(`/static/[\w%.-]+.(css|js)`, &Assets{
			Logger: p.Logger,
			assets: map[string]func(io.Writer) error{
				ui.CSSShort: ui.RenderCSS,
			},
		}),
	}

	return &Router{
		Logger:    p.Logger,
		Store:     p.Store,
		Generator: generator,

		routes: routes,
	}
}
