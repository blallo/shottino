package api

import (
	"bytes"
	"net/http"

	"git.sr.ht/~blallo/logz/interface"

	"git.autistici.org/blallo/shottino/ui"
)

var logMapHome = map[string]interface{}{
	"handler": "Home",
}

type Home struct {
	logz.Logger

	body []byte
}

func (h *Home) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		msg := logData(logMapHome, log{"msg", "method not allowed"})
		h.Logger.Debug(msg)
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	msg := logData(logMapHome, log{"msg", "home requested"})
	h.Logger.Debug(msg)
	w.Header().Set("Content-Type", "text/html")
	w.Write(h.body)
}

type homeHandlerParams struct {
	logz.Logger
	baseURL string
}

func newHomeHandler(p *homeHandlerParams) http.Handler {
	body := bytes.NewBuffer(nil)
	err := ui.RenderHome(body, p.baseURL)
	if err != nil {
		panic(err)
	}

	return &Home{
		Logger: p.Logger,
		body:   body.Bytes(),
	}
}
