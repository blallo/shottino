package api

import (
	"io"
	"net/http"
	"path"
	"path/filepath"
	"strings"

	"git.sr.ht/~blallo/logz/interface"
)

var logMapAssets = map[string]interface{}{"handler": "Assets"}

type Assets struct {
	logz.Logger
	assets map[string]func(io.Writer) error
}

func (h *Assets) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		msg := logData(logMapShortener, log{"msg", "method not allowed"})
		h.Logger.Debug(msg)
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	asset := path.Base(r.URL.Path)
	msg := logData(
		logMapAssets,
		log{"asset", asset},
		log{"path", r.URL.Path},
	)
	msg["msg"] = "asset requested"
	h.Logger.Debug(msg)

	render, ok := h.assets[asset]
	if !ok {
		msg["msg"] = "asset not found"
		h.Logger.Debug(msg)
		http.Error(w, "Asset not found", http.StatusNotFound)
	}

	contentType := contentTypeFromExt(r.URL.Path)
	h.Logger.Debug(logData(msg, log{"msg", "content type determined"}, log{"content_type", contentType}))
	w.Header().Set("Content-Type", contentType)

	// from: https://grayduck.mn/2021/09/13/cache-control-recommendations/
	w.Header().Set("Cache-Control", "max-age=31536000, immutable")

	if err := render(w); err != nil {
		msg["msg"] = "failed to render asset"
		h.Logger.Warn(msg)
	}
}

func contentTypeFromExt(fullName string) string {
	ext := filepath.Ext(fullName)
	switch strings.ToLower(ext) {
	case ".css":
		return "text/css"
	case ".js":
		return "application/javascript"
	default:
		return "text/plain"
	}
}
