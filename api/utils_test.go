//go:build !integration

package api

import (
	"context"
	"net/url"
	"time"

	"git.autistici.org/blallo/shottino"
)

type mockStore struct {
	expected map[string]string
}

func (m *mockStore) Get(ctx context.Context, short string) (*url.URL, error) {
	addr, ok := m.expected[short]
	if !ok {
		return nil, nil
	}

	return url.Parse(addr)
}

func (m *mockStore) Set(ctx context.Context, short string, new *url.URL) error {
	m.expected[short] = new.String()
	return nil
}

func (m *mockStore) Vacuum(ctx context.Context, since time.Duration) error {
	m.expected = make(map[string]string)
	return nil
}

var _ shottino.Store = &mockStore{}

type mockGenerator struct {
	genList []string
}

func (m *mockGenerator) Do() string {
	res := m.genList[0]
	m.genList = m.genList[1:]

	return res
}

func (m *mockGenerator) Charset() string {
	return "a-zA-Z0-9_-"
}

var _ shottino.Generator = &mockGenerator{}
