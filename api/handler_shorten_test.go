//go:build !integration

package api

import (
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	"git.sr.ht/~blallo/logz/interface"
	"git.sr.ht/~blallo/logz/jsonlogger"
)

func TestShortener(t *testing.T) {
	handler := &Shortener{
		Store: &mockStore{
			expected: map[string]string{
				"short123": "https://example.com/the/long/address",
			},
		},
		Logger:     jsonlogger.New().SetLevel(logz.LogDebug),
		Generator:  &mockGenerator{[]string{"short123", "short234", "json1234"}},
		ValidURLRe: regexp.MustCompile("^https?://.*$"),
	}

	req := httptest.NewRequest(http.MethodPost, "/api/shorten", nil)
	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp := w.Result()

	if resp.StatusCode != http.StatusBadRequest {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}

	body := strings.NewReader(`address=https%3A%2F%2Fexample.com%2Fvery%2Flong%2Faddress`)
	req = httptest.NewRequest(http.MethodPost, "/api/shorten", body)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp = w.Result()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}

	body = strings.NewReader(`{"address":"https://example.com/another/long/address"}`)
	req = httptest.NewRequest(http.MethodPost, "/api/shorten", body)
	req.Header.Set("Content-Type", "application/json")
	w = httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp = w.Result()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected status: %d", resp.StatusCode)
	}
}
