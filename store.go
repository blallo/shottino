package shottino

import (
	"context"
	"net/url"
	"time"
)

type Store interface {
	Set(context.Context, string, *url.URL) error
	Get(context.Context, string) (*url.URL, error)
	Vacuum(context.Context, time.Duration) error
}

type ErrAlreadyThere struct {
	Short string
}

func (e *ErrAlreadyThere) Error() string {
	return "url already shortened"
}
