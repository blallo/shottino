package shottino

type Generator interface {
	Do() string
	Charset() string
}
