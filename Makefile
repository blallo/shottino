DB_URI ?= mysql://shottino:password@tcp(127.0.0.1:3306)/shottino

./ui/generated.go:
	cd ui && go generate

./bin/shottino: ./ui/generated.go
	go build -tags=all -o ./bin/shottino ./cmd/shottino/...

.PHONY: clean
clean:
	rm -rf ./ui/assets
	rm -rf ./ui/generated.go
	rm -f ./bin/shottino

.PHONY: build
build: clean ./bin/shottino

.PHONY: tests-%
tests-%:
	go test -tags=$* -count=1 -v ./...

.PHONY: tests
tests: tests-all

.PHONY: bench
bench:
	cd generator && \
	go test -bench . -benchmem

.PHONY: integration-tests-%
integration-tests-%:
	docker-compose up -d
	sleep 2
	go test -v -tags="integration $*" ./...
	docker-compose stop

.PHONY: integration-tests
integration-tests: integration-tests-all

.PHONY: run
run: ./bin/shottino
	docker-compose up -d
	sleep 2
	if [ "$(BASE_URL)" != "" ]; then FLAGS="$${FLAGS} -base-url $(BASE_URL)"; fi && \
	if [ "$(VALID_URL)" != "" ]; then FLAGS="$${FLAGS} -valid-url $(VALID_URL)"; fi && \
	./bin/shottino -db-uri "$(DB_URI)" -level debug $${FLAGS}
