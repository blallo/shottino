//go:build !integration

package caching

import (
	"context"
	"net/url"
	"testing"
	"time"

	"git.autistici.org/blallo/shottino"
)

var (
	ttl   = time.Hour
	cycle = 10 * time.Minute
	u, _  = url.Parse("https://example.com")
)

func TestStore(t *testing.T) {
	ctx := context.TODO()
	wrapped := &mockStore{}
	store := NewStore(wrapped, ttl, cycle)

	err := store.Set(ctx, "test", u)
	if err != nil {
		t.Fatal(err)
	}
	if !wrapped.set {
		t.Fatal("wrapped should have been set")
	}

	wrapped.set = false
	err = store.Set(ctx, "test2", u)
	if err != nil {
		if _, ok := err.(*shottino.ErrAlreadyThere); !ok {
			t.Fatal(err)
		}
	} else {
		t.Fatal("should have errored")
	}
	if wrapped.set {
		t.Fatal("wrapped should have NOT been set")
	}

	wrapped.get = false
	val, err := store.Get(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if val.String() != u.String() {
		t.Fatalf("unexpected val '%s', expected '%s'", val.String(), u.String())
	}
	if wrapped.get {
		t.Fatal("wrapped should have NOT been queried")
	}

	wrapped.get = false
	val, err = store.Get(ctx, "not_there")
	if err != nil {
		t.Fatal(err)
	}
	if val != nil {
		t.Fatalf("unexpected value '%s'", val)
	}
	if !wrapped.get {
		t.Fatal("wrapped should have been queried")
	}

	wrapped.get = false
	err = store.Vacuum(ctx, 0)
	if err != nil {
		t.Fatal(err)
	}

	val, err = store.Get(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if val.String() != u.String() {
		t.Fatalf("unexpected val '%s', expected '%s'", val.String(), u.String())
	}
	if !wrapped.get {
		t.Fatal("wrapped should have been touched")
	}
}
