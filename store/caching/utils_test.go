//go:build !integration

package caching

import (
	"context"
	"net/url"
	"time"

	"git.autistici.org/blallo/shottino"
)

type mockStore struct {
	set   bool
	get   bool
	value *url.URL
	key   string
}

func (s *mockStore) Set(ctx context.Context, key string, value *url.URL) error {
	s.set = true
	s.key = key
	s.value = value

	return nil
}

func (s *mockStore) Get(ctx context.Context, key string) (*url.URL, error) {
	s.get = true
	if key != s.key {
		return nil, nil
	}

	return s.value, nil
}

func (s *mockStore) Vacuum(ctx context.Context, d time.Duration) error {
	s.set = false
	s.get = false
	s.key = ""
	s.value = nil

	return nil
}

var _ shottino.Store = &mockStore{}
