package caching

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"github.com/patrickmn/go-cache"

	"git.autistici.org/blallo/shottino"
)

type Store struct {
	shorts  *cache.Cache
	urls    *cache.Cache
	wrapped shottino.Store
	ttl     time.Duration
	cycle   time.Duration
}

func (s *Store) Set(ctx context.Context, key string, value *url.URL) error {
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("set timeout: %w", ctx.Err())
		case err := <-s.set(ctx, key, value):
			return err
		}
	}
}

func (s *Store) Get(ctx context.Context, key string) (*url.URL, error) {
	for {
		select {
		case <-ctx.Done():
			return nil, fmt.Errorf("get timeout: %w", ctx.Err())
		case v := <-s.get(ctx, key):
			return v.value, v.err
		}
	}
}

// set:
// search in urls cache ->
// already set
// ? return value
// : (delegate to wrapped set ->
//   no error
//   ? set cache and return
//   : return error)
func (s *Store) set(ctx context.Context, key string, value *url.URL) chan error {
	ch := make(chan error)

	go func() {
		addr := value.String()

		if val, ok := s.shorts.Get(addr); ok {
			ch <- &shottino.ErrAlreadyThere{Short: val.(string)}
			return
		}

		err := s.wrapped.Set(ctx, key, value)
		if err != nil {
			ch <- err
			return
		}

		s.urls.Set(key, addr, s.ttl)
		s.shorts.Set(addr, key, s.ttl)

		ch <- nil
	}()

	return ch
}

type getResult struct {
	value *url.URL
	err   error
}

// get:
// search in cache ->
// found
// ? return value
// : (search in wrapped ->
//    found
//    ? set cache and return value and no error
//    : (is error ? return nil and error : return nil and no error))
func (s *Store) get(ctx context.Context, key string) chan *getResult {
	ch := make(chan *getResult)

	go func() {
		if val, ok := s.urls.Get(key); ok {
			u, err := url.Parse(val.(string))
			ch <- &getResult{value: u, err: err}
			return
		}

		val, err := s.wrapped.Get(ctx, key)
		if err != nil {
			ch <- &getResult{value: nil, err: err}
			return
		}
		if val == nil {
			ch <- &getResult{value: nil, err: nil}
			return
		}

		addr := val.String()
		s.urls.Set(key, addr, s.ttl)
		s.shorts.Set(addr, key, s.ttl)

		ch <- &getResult{value: val, err: nil}
	}()

	return ch
}

func (s *Store) Vacuum(ctx context.Context, t time.Duration) error {
	done := make(chan struct{})

	go func() {
		s.shorts.Flush()
		done <- struct{}{}
	}()

	go func() {
		s.urls.Flush()
		done <- struct{}{}
	}()

	i := 0
	for i != 2 {
		select {
		case <-ctx.Done():
			return fmt.Errorf("vacuum still in progress: %w", ctx.Err())
		case <-done:
			i++
		}
	}

	return nil
}

func NewStore(wrapped shottino.Store, ttl, cycle time.Duration) shottino.Store {
	return &Store{
		urls:    cache.New(ttl, cycle),
		shorts:  cache.New(ttl, cycle),
		wrapped: wrapped,
		ttl:     ttl,
		cycle:   cycle,
	}
}

var _ shottino.Store = &Store{}
