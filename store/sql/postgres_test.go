//go:build (postgres || all) && !integration

package sql

import (
	"fmt"
	"testing"
)

func TestPostgresDispatcher(t *testing.T) {
	dispatcher := pgDispatcher{}

	connStr := `user:password@addr:1234/db_name`
	fullConnStr := fmt.Sprintf("postgres://%s", connStr)

	if res, err := dispatcher.parse(fullConnStr); err != nil {
		t.Fatal(err)
	} else {
		if res != fullConnStr {
			t.Fatalf("unexpected connection string: %s", res)
		}
	}

	otherAllowedStr := fmt.Sprintf("postgresql://%s", connStr)

	if res, err := dispatcher.parse(otherAllowedStr); err != nil {
		t.Fatal(err)
	} else {
		if res != fullConnStr {
			t.Fatalf("unexpected connection string: %s", res)
		}
	}

	if _, err := dispatcher.parse(connStr); err == nil {
		t.Fatal("should have failed")
	}
}

func TestPgRebind(t *testing.T) {
	const st1 statement = `this is a {} test statement`
	res := st1.rebind(pgRebind)
	if res != `this is a $1 test statement` {
		t.Fatalf("unexpected result: %s", res)
	}

	const st2 statement = `this is a {} test statement {}, {}`
	res = st2.rebind(pgRebind)
	if res != `this is a $1 test statement $2, $3` {
		t.Fatalf("unexpected result: %s", res)
	}
}
