package sql

import (
	"strings"
)

type BindFunc func([]string) string

type statement string

func (s statement) tokenize() []string {
	return strings.Split(string(s), "{}")
}

func (s statement) rebind(fn BindFunc) string {
	return fn(s.tokenize())
}

const (
	getQuery    statement = `SELECT url FROM store WHERE short={}`
	getUrlQuery statement = `SELECT short FROM store WHERE url={}`
	setQuery    statement = `INSERT INTO store (short, url, created_at) VALUES ({}, {}, {})`
	vacuumQuery statement = `DELETE FROM store WHERE created_at < {}`
	upVersion   statement = `INSERT INTO version (version, created_at) VALUES ({}, {})`
	downVersion statement = `DELETE FROM version WHERE version={}`

	// This is constant
	versionQuery string = `SELECT version FROM version ORDER BY version DESC LIMIT 1`
)

type statements struct {
	getQuery    string
	getUrlQuery string
	setQuery    string
	vacuumQuery string
	upVersion   string
	downVersion string

	// constant statements
	versionQuery string
}

func initStatements(fn BindFunc) *statements {
	s := statements{}

	s.getQuery = getQuery.rebind(fn)
	s.getUrlQuery = getUrlQuery.rebind(fn)
	s.setQuery = setQuery.rebind(fn)
	s.vacuumQuery = vacuumQuery.rebind(fn)
	s.upVersion = upVersion.rebind(fn)
	s.downVersion = downVersion.rebind(fn)

	return &s
}
