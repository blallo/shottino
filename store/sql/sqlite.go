//go:build sqlite || all

package sql

import (
	"database/sql"
	"fmt"
	"regexp"
	"strings"

	_ "github.com/mattn/go-sqlite3"

	"git.autistici.org/blallo/shottino"
)

var (
	reSQLite         = regexp.MustCompile(`^sqlite://(.*)$`)
	sqliteStatements = initStatements(sqliteRebind)
)

type sqliteDispatcher struct{}

func (m sqliteDispatcher) newStore(uri string) (shottino.Store, error) {
	return newSQLiteStore(uri)
}

func (m sqliteDispatcher) parse(uri string) (string, error) {
	res := reSQLite.FindStringSubmatch(uri)
	if len(res) != 2 {
		return "", fmt.Errorf("sql: malformed uri: %s", uri)
	}
	return res[1], nil
}

var _ uriDispatcher = sqliteDispatcher{}

func init() {
	if newStoreMap == nil {
		newStoreMap = make(map[*regexp.Regexp]uriDispatcher)
	}
	newStoreMap[reSQLite] = sqliteDispatcher{}
}

func newSQLiteStore(uri string) (shottino.Store, error) {
	db, err := sql.Open("sqlite3", uri)
	if err != nil {
		return nil, fmt.Errorf("sqlite: error when creating the connection: %s", err)
	}

	version := GetVersion(db)

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	if err := Migrate(tx, sqliteMigrations, version); err != nil {
		return nil, fmt.Errorf("sqlite: error with schema creation/update: %s", err)
	}

	if err = tx.Commit(); err != nil {
		return nil, err
	}

	return &Store{
		db: db,
		st: sqliteStatements,
	}, nil
}

func newSQLiteStoreTx(tx *sql.Tx) (*Store, error) {
	if err := Migrate(tx, sqliteMigrations, 0); err != nil {
		return nil, fmt.Errorf("sqlite: error with schema creation/update: %s", err)
	}

	return &Store{
		tx: tx,
		st: sqliteStatements,
	}, nil
}

func sqliteRebind(tokens []string) string {
	return strings.Join(tokens, "?")
}

var sqliteMigrations = Migrations{
	Rebind: sqliteRebind,
	Migrations: []*Migration{
		{
			Up: []string{
				`CREATE TABLE IF NOT EXISTS version (
            id INT AUTO_INCREMENT PRIMARY KEY,
            version INT,
            created_at INT
        )`,
				`CREATE TABLE IF NOT EXISTS store (
            id INT AUTO_INCREMENT PRIMARY KEY,
            short VARCHAR(10) UNIQUE,
            url VARCHAR(2048) UNIQUE,
            created_at INT
        )`,
			},
			Down: []string{
				`DROP TABLE IF EXISTS store`,
				`DROP TABLE IF EXISTS version`,
			},
		},
		{
			Up: []string{
				`CREATE UNIQUE INDEX short_idx ON store (short)`,
				`CREATE UNIQUE INDEX url_idx ON store (url)`,
				`CREATE UNIQUE INDEX pairs_idx ON store (short, url)`,
			},
			Down: []string{
				`DROP INDEX pairs_idx`,
				`DROP INDEX url_idx`,
				`DROP INDEX short_idx`,
			},
		},
	},
}
