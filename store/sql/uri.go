package sql

import (
	"fmt"
	"regexp"

	"git.autistici.org/blallo/shottino"
)

type uriDispatcher interface {
	newStore(string) (shottino.Store, error)
	parse(string) (string, error)
}

var newStoreMap map[*regexp.Regexp]uriDispatcher

func NewStore(uri string) (shottino.Store, error) {
	for re, dispatcher := range newStoreMap {
		if re.MatchString(uri) {
			mangled, err := dispatcher.parse(uri)
			if err != nil {
				return nil, err
			}
			return dispatcher.newStore(mangled)
		}
	}

	return nil, fmt.Errorf("sql: no store could be created for given uri: %s", uri)
}
