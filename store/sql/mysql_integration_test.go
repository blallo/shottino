//go:build integration && (mysql || all)

package sql

import (
	"context"
	"database/sql"
	"net/url"
	"reflect"
	"testing"

	"git.autistici.org/blallo/shottino"
)

func TestMySQLStore(t *testing.T) {
	uri := "shottino:password@tcp(localhost:3306)/shottino"
	ctx := context.TODO()
	short := "12345"
	expected, _ := url.Parse("https://example.blog/f1Rs7_p0S7")

	db, err := sql.Open("mysql", uri)
	if err != nil {
		t.Fatal(err)
	}

	tx, err := db.Begin()
	if err != nil {
		t.Fatal(err)
	}

	store, err := newMySQLStoreTx(tx)
	if err != nil {
		t.Fatal(err)
	}

	res, err := store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res != nil {
		t.Errorf("expected nil, got: %s", res)
	}

	err = store.SetInTx(ctx, short, expected)
	if err != nil {
		t.Fatal(err)
	}

	res, err = store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res.String() != expected.String() {
		t.Fatalf("expected: %s\tgot: %s", expected, res)
	}

	expErr := &shottino.ErrAlreadyThere{Short: short}
	err = store.SetInTx(ctx, "1312", expected)
	if !reflect.DeepEqual(err, expErr) {
		t.Fatalf("unexpected error: %s", err)
	}

	if err := Revert(tx, mysqlMigrations, len(mysqlMigrations.Migrations), 0); err != nil {
		t.Fatal(err)
	}
	tx.Rollback()
}
