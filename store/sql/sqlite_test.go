//go:build (sqlite || all) && !integration

package sql

import (
	"fmt"
	"testing"
)

func TestSQLiteDispatcher(t *testing.T) {
	dispatcher := sqliteDispatcher{}

	connStr := `/path/to/shottino.sqlite`
	successCase := fmt.Sprintf("sqlite://%s", connStr)

	if res, err := dispatcher.parse(successCase); err != nil {
		t.Fatal(err)
	} else {
		if res != connStr {
			t.Fatalf("unexpected connection string: %s", res)
		}
	}

	if _, err := dispatcher.parse(connStr); err == nil {
		t.Fatal("should have failed")
	}
}

func TestSQLiteRebind(t *testing.T) {
	const st1 statement = `this is a {} test statement`
	res := st1.rebind(sqliteRebind)
	if res != `this is a ? test statement` {
		t.Fatalf("unexpected result: %s", res)
	}

	const st2 statement = `this is a {} test statement {}, {}`
	res = st2.rebind(sqliteRebind)
	if res != `this is a ? test statement ?, ?` {
		t.Fatalf("unexpected result: %s", res)
	}
}
