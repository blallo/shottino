package sql

import (
	"database/sql"
	"fmt"
	"time"
)

type Migration struct {
	Up   []string
	Down []string
}

type Migrations struct {
	Rebind     BindFunc
	Migrations []*Migration
}

func Migrate(tx *sql.Tx, m Migrations, version int) error {
	latest := len(m.Migrations)
	upVersionInsert := upVersion.rebind(m.Rebind)

	if version > latest {
		return fmt.Errorf("detected version: %d; this binary knows up to version: %d", version, latest)
	}

	if version < latest {
		for i := version; i < latest; i++ {
			for _, statement := range m.Migrations[i].Up {
				if _, err := tx.Exec(statement); err != nil {
					return fmt.Errorf("sql: failed executing statement ('%s'): %w", statement, err)
				}
			}
			_, err := tx.Exec(upVersionInsert, i+1, time.Now().Unix())
			if err != nil {
				return fmt.Errorf(
					"sql: failed up'ing to version (%d, %s): %w", i+1, upVersionInsert, err,
				)
			}
		}
	}

	return nil
}

func Revert(tx *sql.Tx, m Migrations, version, until int) error {
	for i := version; i > until; i-- {
		for _, statement := range m.Migrations[i-1].Down {
			tx.Exec(downVersion.rebind(m.Rebind), i)
			if _, err := tx.Exec(statement); err != nil {
				return fmt.Errorf("sql: failed executing statement ('%s'): %w", statement, err)
			}
		}
	}

	return nil
}

func GetVersion(db *sql.DB) (version int) {
	err := db.QueryRow(versionQuery).Scan(&version)
	if err != nil {
		version = 0
	}

	return version
}
