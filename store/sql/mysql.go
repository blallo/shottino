//go:build mysql || all

package sql

import (
	"database/sql"
	"fmt"
	"regexp"
	"strings"

	_ "github.com/go-sql-driver/mysql"

	"git.autistici.org/blallo/shottino"
)

var (
	reMysql         = regexp.MustCompile(`^mysql://(.*)$`)
	mysqlStatements = initStatements(mysqlRebind)
)

type mysqlDispatcher struct{}

func (m mysqlDispatcher) newStore(uri string) (shottino.Store, error) {
	return newMySQLStore(uri)
}

func (m mysqlDispatcher) parse(uri string) (string, error) {
	res := reMysql.FindStringSubmatch(uri)
	if len(res) != 2 {
		return "", fmt.Errorf("sql: malformed uri: %s", uri)
	}
	return res[1], nil
}

var _ uriDispatcher = mysqlDispatcher{}

func init() {
	if newStoreMap == nil {
		newStoreMap = make(map[*regexp.Regexp]uriDispatcher)
	}
	newStoreMap[reMysql] = mysqlDispatcher{}
}

func newMySQLStore(uri string) (shottino.Store, error) {
	db, err := sql.Open("mysql", uri)
	if err != nil {
		return nil, fmt.Errorf("mysql: error when creating the connection: %s", err)
	}

	version := GetVersion(db)

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	if err := Migrate(tx, mysqlMigrations, version); err != nil {
		return nil, fmt.Errorf("mysql: error with schema creation/update: %s", err)
	}

	if err = tx.Commit(); err != nil {
		return nil, err
	}

	return &Store{
		db: db,
		st: mysqlStatements,
	}, nil
}

func newMySQLStoreTx(tx *sql.Tx) (*Store, error) {
	if err := Migrate(tx, mysqlMigrations, 0); err != nil {
		return nil, fmt.Errorf("mysql: error with schema creation/update: %s", err)
	}

	return &Store{
		tx: tx,
		st: mysqlStatements,
	}, nil
}

func mysqlRebind(tokens []string) string {
	return strings.Join(tokens, "?")
}

var mysqlMigrations = Migrations{
	Rebind: mysqlRebind,
	Migrations: []*Migration{
		{
			Up: []string{
				`CREATE TABLE IF NOT EXISTS version (
            id INT AUTO_INCREMENT PRIMARY KEY,
            version INT,
            created_at INT
        )`,
				`CREATE TABLE IF NOT EXISTS store (
            id INT AUTO_INCREMENT PRIMARY KEY,
            short VARCHAR(10) UNIQUE,
            url VARCHAR(2048) UNIQUE,
            created_at INT
        )`,
			},
			Down: []string{
				`DROP TABLE IF EXISTS store`,
				`DROP TABLE IF EXISTS version`,
			},
		},
		{
			Up: []string{
				`CREATE UNIQUE INDEX short_idx ON store (short)`,
				`CREATE UNIQUE INDEX url_idx ON store (url)`,
				`CREATE UNIQUE INDEX pairs_idx ON store (short, url)`,
			},
			Down: []string{
				`DROP INDEX pairs_idx ON store`,
				`DROP INDEX url_idx ON store`,
				`DROP INDEX short_idx ON store`,
			},
		},
	},
}
