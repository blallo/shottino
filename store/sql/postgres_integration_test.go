//go:build integration && (postgres || all)

package sql

import (
	"context"
	"database/sql"
	"net/url"
	"reflect"
	"testing"

	"git.autistici.org/blallo/shottino"
)

func TestPostgresStore(t *testing.T) {
	uri := "postgres://shottino:password@localhost:5432/shottino?sslmode=disable"
	ctx := context.TODO()
	short := "12345"
	expected, _ := url.Parse("https://example.blog/f1Rs7_p0S7")

	db, err := sql.Open("pgx", uri)
	if err != nil {
		t.Fatal(err)
	}

	tx, err := db.Begin()
	if err != nil {
		t.Fatal(err)
	}

	store, err := newPostgresStoreTx(tx)
	if err != nil {
		t.Fatal(err)
	}

	res, err := store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res != nil {
		t.Errorf("expected nil, got: %s", res)
	}

	err = store.SetInTx(ctx, short, expected)
	if err != nil {
		t.Fatal(err)
	}

	res, err = store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res.String() != expected.String() {
		t.Fatalf("expected: %s\tgot: %s", expected, res)
	}

	expErr := &shottino.ErrAlreadyThere{Short: short}
	err = store.SetInTx(ctx, "1312", expected)
	if !reflect.DeepEqual(err, expErr) {
		t.Fatalf("unexpected error: %s", err)
	}

	if err := Revert(tx, pgMigrations, len(pgMigrations.Migrations), 0); err != nil {
		t.Fatal(err)
	}
	tx.Rollback()
}
