package sql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net/url"
	"time"

	"git.autistici.org/blallo/shottino"
)

type Store struct {
	db *sql.DB
	tx *sql.Tx
	st *statements
}

func (s *Store) WithTx(tx *sql.Tx) {
	s.tx = tx
}

func (s *Store) Get(ctx context.Context, short string) (*url.URL, error) {
	return s.get(ctx, short, s.db.QueryRowContext)
}

func (s *Store) GetInTx(ctx context.Context, short string) (*url.URL, error) {
	return s.get(ctx, short, s.tx.QueryRowContext)
}

func (s *Store) get(
	ctx context.Context,
	short string,
	query func(context.Context, string, ...interface{}) *sql.Row,
) (*url.URL, error) {
	var urlStr string

	err := query(ctx, s.st.getQuery, short).Scan(&urlStr)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("sql: failed to query: %s", err)
	}

	return url.Parse(urlStr)
}

func (s *Store) Set(ctx context.Context, short string, u *url.URL) error {
	var err error

	s.tx, err = s.db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		return fmt.Errorf("sql: failed to enter transaction: %s", err)
	}
	defer s.tx.Rollback()
	defer func() { s.tx = nil }()

	err = s.SetInTx(ctx, short, u)
	if err != nil {
		return err
	}

	err = s.tx.Commit()
	if err != nil {
		return fmt.Errorf("sql: failed to commit transaction: %s", err)
	}
	return nil
}

func (s *Store) SetInTx(ctx context.Context, short string, u *url.URL) error {
	var oldShort string
	now := time.Now().Unix()
	found := true

	err := s.tx.QueryRowContext(ctx, s.st.getUrlQuery, u.String()).Scan(&oldShort)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return fmt.Errorf("sql: transaction error: %s", err)
		}
		found = false
	}
	if found {
		return &shottino.ErrAlreadyThere{
			Short: oldShort,
		}
	}

	_, err = s.tx.ExecContext(ctx, s.st.setQuery, short, u.String(), now)
	if err != nil {
		return fmt.Errorf("sql: failed to set: %s", err)
	}

	return nil
}

func (s *Store) Vacuum(ctx context.Context, interval time.Duration) error {
	return s.vacuum(ctx, interval, s.db.ExecContext)
}

func (s *Store) VacuumInTx(ctx context.Context, interval time.Duration) error {
	return s.vacuum(ctx, interval, s.tx.ExecContext)
}

func (s *Store) vacuum(
	ctx context.Context,
	interval time.Duration,
	exec func(context.Context, string, ...interface{}) (sql.Result, error),
) error {
	since := time.Now().Add(-interval).Unix()

	_, err := exec(ctx, s.st.vacuumQuery, since)
	if err != nil {
		return fmt.Errorf("sql: failed to vacuum: %s", err)
	}

	return nil
}

var _ shottino.Store = &Store{}
