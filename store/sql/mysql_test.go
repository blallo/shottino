//go:build (mysql || all) && !integration

package sql

import (
	"fmt"
	"testing"
)

func TestMySQLDispatcher(t *testing.T) {
	dispatcher := mysqlDispatcher{}

	connStr := `user:password@addr:1234/db_name`
	successCase := fmt.Sprintf("mysql://%s", connStr)

	if res, err := dispatcher.parse(successCase); err != nil {
		t.Fatal(err)
	} else {
		if res != connStr {
			t.Fatalf("unexpected connection string: %s", res)
		}
	}

	if _, err := dispatcher.parse(connStr); err == nil {
		t.Fatal("should have failed")
	}
}

func TestMySQLRebind(t *testing.T) {
	const st1 statement = `this is a {} test statement`
	res := st1.rebind(mysqlRebind)
	if res != `this is a ? test statement` {
		t.Fatalf("unexpected result: %s", res)
	}

	const st2 statement = `this is a {} test statement {}, {}`
	res = st2.rebind(mysqlRebind)
	if res != `this is a ? test statement ?, ?` {
		t.Fatalf("unexpected result: %s", res)
	}
}
