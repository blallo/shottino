//go:build postgres || all

package sql

import (
	"database/sql"
	"fmt"
	"regexp"

	_ "github.com/jackc/pgx/stdlib"

	"git.autistici.org/blallo/shottino"
)

var (
	rePg         = regexp.MustCompile(`^postgres(ql)?://(.*)$`)
	pgStatements = initStatements(pgRebind)
)

type pgDispatcher struct{}

func (m pgDispatcher) newStore(uri string) (shottino.Store, error) {
	return newPostgresStore(uri)
}

func (m pgDispatcher) parse(uri string) (string, error) {
	res := rePg.FindStringSubmatch(uri)
	switch len(res) {
	case 2: // matching postgres://.*
		return fmt.Sprintf("postgres://%s", res[1]), nil
	case 3: // matching postgresql://.*
		return fmt.Sprintf("postgres://%s", res[2]), nil
	default:
		return "", fmt.Errorf("sql: malformed uri: %s", uri)
	}
}

var _ uriDispatcher = pgDispatcher{}

func init() {
	if newStoreMap == nil {
		newStoreMap = make(map[*regexp.Regexp]uriDispatcher)
	}
	newStoreMap[rePg] = pgDispatcher{}
}

func newPostgresStore(uri string) (shottino.Store, error) {
	db, err := sql.Open("pgx", uri)
	if err != nil {
		return nil, fmt.Errorf("postgres: error when creating the connection: %w", err)
	}

	version := GetVersion(db)

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	if err := Migrate(tx, pgMigrations, version); err != nil {
		return nil, fmt.Errorf("postgres: error with schema creation/update: %w", err)
	}

	if err = tx.Commit(); err != nil {
		return nil, err
	}

	return &Store{
		db: db,
		st: pgStatements,
	}, nil
}

func newPostgresStoreTx(tx *sql.Tx) (*Store, error) {
	if err := Migrate(tx, pgMigrations, 0); err != nil {
		return nil, fmt.Errorf("postgres: error with schema creation/update: %w", err)
	}

	return &Store{
		tx: tx,
		st: pgStatements,
	}, nil
}

func pgRebind(tokens []string) string {
	result := tokens[0]

	for i, token := range tokens {
		if i == 0 {
			continue
		}
		result = fmt.Sprintf("%s$%d%s", result, i, token)
	}

	return result
}

var pgMigrations = Migrations{
	Rebind: pgRebind,
	Migrations: []*Migration{
		{
			Up: []string{
				`CREATE TABLE IF NOT EXISTS version (
            id SERIAL PRIMARY KEY,
            version INTEGER,
            created_at INTEGER
        )`,
				`CREATE TABLE IF NOT EXISTS store (
            id SERIAL PRIMARY KEY,
            short TEXT UNIQUE,
            url TEXT UNIQUE,
            created_at INTEGER
        )`,
			},
			Down: []string{
				`DROP TABLE IF EXISTS store`,
				`DROP TABLE IF EXISTS version`,
			},
		},
		{
			Up: []string{
				`CREATE UNIQUE INDEX short_idx ON store (short)`,
				`CREATE UNIQUE INDEX url_idx ON store (url)`,
				`CREATE UNIQUE INDEX pairs_idx ON store (short, url)`,
			},
			Down: []string{
				`DROP INDEX pairs_idx`,
				`DROP INDEX url_idx`,
				`DROP INDEX short_idx`,
			},
		},
	},
}
