//go:build integration && (sqlite || all)

package sql

import (
	"context"
	"database/sql"
	"net/url"
	"path/filepath"
	"reflect"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"git.autistici.org/blallo/shottino"
)

func TestSQLiteStore(t *testing.T) {
	dir := t.TempDir()
	uri := filepath.Join(dir, "shottino.db")
	ctx := context.TODO()
	short := "12345"
	expected, _ := url.Parse("https://example.blog/f1Rs7_p0S7")

	db, err := sql.Open("sqlite3", uri)
	if err != nil {
		t.Fatal(err)
	}

	tx, err := db.Begin()
	if err != nil {
		t.Fatal(err)
	}

	store, err := newSQLiteStoreTx(tx)
	if err != nil {
		t.Fatal(err)
	}

	res, err := store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res != nil {
		t.Errorf("expected nil, got: %s", res)
	}

	err = store.SetInTx(ctx, short, expected)
	if err != nil {
		t.Fatal(err)
	}

	res, err = store.GetInTx(ctx, short)
	if err != nil {
		t.Fatal(err)
	}
	if res.String() != expected.String() {
		t.Fatalf("expected: %s\tgot: %s", expected, res)
	}

	expErr := &shottino.ErrAlreadyThere{Short: short}
	err = store.SetInTx(ctx, "1312", expected)
	if !reflect.DeepEqual(err, expErr) {
		t.Fatalf("unexpected error: %s", err)
	}

	if err := Revert(tx, sqliteMigrations, len(sqliteMigrations.Migrations), 0); err != nil {
		t.Fatal(err)
	}
	tx.Rollback()
}
