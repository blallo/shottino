package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"time"

	"git.sr.ht/~blallo/logz/interface"
	"git.sr.ht/~blallo/logz/jsonlogger"

	"git.autistici.org/blallo/shottino/api"
	"git.autistici.org/blallo/shottino/store/caching"
	"git.autistici.org/blallo/shottino/store/sql"
)

var (
	ttl   = 24 * time.Hour
	cycle = 10 * time.Minute
	urlRe = regexp.MustCompile("^https?://.*$")
)

func main() {
	var shortLen, bindPort int
	var bindAddr, dbUri, baseURL, logLevel, validURL string

	flag.IntVar(&shortLen, "short-len", 8, "set the length of the generated short sub-path (min. 6)")
	flag.StringVar(&bindAddr, "bind-addr", "0.0.0.0", "set the address to bind to")
	flag.IntVar(&bindPort, "bind-port", 7070, "set the port to bind to")
	flag.StringVar(&dbUri, "db-uri", "", "set the connection string for the db")
	flag.StringVar(&baseURL, "base-url", "", "set the base url")
	flag.StringVar(&logLevel, "level", "", "set the logging level")
	flag.StringVar(&validURL, "valid-url", "^https?://.*$", "set a regexp to limit allowed shortable urls")

	flag.Parse()

	log := jsonlogger.New()
	if logLevel != "" {
		level, err := logz.ToLevel(logLevel)
		if err != nil {
			log.Err(map[string]interface{}{
				"msg": "invalid log level",
				"err": err.Error(),
			})
			os.Exit(1)
		}

		log.SetLevel(level)
	}

	validateLen(log, shortLen)
	addr := validateAndFormatAddr(log, bindAddr, bindPort)
	validateDBUri(log, dbUri)
	baseURLSafe := parseAndValidateBaseURL(log, baseURL)
	validURLRe := parseAndValidateValidURLRe(log, validURL)

	st, err := sql.NewStore(dbUri)
	if err != nil {
		log.Err(map[string]interface{}{"msg": "failed to connect to the db", "err": err.Error()})
		os.Exit(2)
	}
	st = caching.NewStore(st, ttl, cycle)

	router := api.NewRouter(&api.RouterParams{
		Logger:     log,
		Store:      st,
		ShortLen:   shortLen,
		BaseURL:    baseURLSafe,
		ValidURLRe: validURLRe,
	})

	log.Info(map[string]interface{}{
		"msg":  "shottino started",
		"addr": addr,
	})
	err = http.ListenAndServe(addr, router)

	lvl := logz.LogInfo
	msg := map[string]interface{}{
		"msg": "program exited",
	}
	if err != nil {
		lvl = logz.LogErr
		msg["err"] = err
	}
	log.Log(lvl, msg)
}

func validateLen(log logz.Logger, shortLen int) {
	if shortLen < 6 {
		log.Err(map[string]interface{}{
			"msg":      "shortLen too short",
			"shortLen": shortLen,
		})
		os.Exit(1)
	}
}

func validateAndFormatAddr(log logz.Logger, bindAddr string, bindPort int) string {
	ip := net.ParseIP(bindAddr)
	if ip == nil {
		log.Err(map[string]interface{}{
			"msg": "missing or invalid bindAddr",
		})
		os.Exit(1)
	}

	if bindPort < 1 || bindPort > 65535 {
		log.Err(map[string]interface{}{
			"msg": "invalid port",
		})
		os.Exit(1)
	}

	return fmt.Sprintf("%s:%d", ip, bindPort)
}

func validateDBUri(log logz.Logger, uri string) {
	if uri == "" {
		log.Err(map[string]interface{}{
			"msg": "missing db uri",
		})
		os.Exit(3)
	}
}

func parseAndValidateBaseURL(log logz.Logger, baseURL string) string {
	if baseURL == "" {
		return ""
	}

	if !urlRe.MatchString(baseURL) {
		baseURL = "http://" + baseURL
	}

	addr, err := url.Parse(baseURL)
	if err != nil {
		log.Err(map[string]interface{}{
			"msg":     "malformed base url",
			"err":     err.Error(),
			"baseURL": baseURL,
		})
		os.Exit(1)
	}

	log.Debug(map[string]interface{}{
		"msg":     "base url parsed",
		"baseURL": addr.String(),
	})
	return addr.String()
}

func parseAndValidateValidURLRe(log logz.Logger, validURL string) *regexp.Regexp {
	re, err := regexp.Compile(validURL)
	if err != nil {
		log.Err(map[string]interface{}{
			"msg":      "invalid regular expression given",
			"err":      err.Error(),
			"validURL": validURL,
		})
		os.Exit(1)
	}

	log.Debug(map[string]interface{}{
		"msg":    "valid url regexp generated",
		"regexp": re.String(),
	})
	return re
}
