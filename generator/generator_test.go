//go:build !integration

package generator

import (
	"fmt"
	"testing"
)

func TestGenerator(t *testing.T) {
	fn := func(N int) func(*testing.T) {
		return func(t *testing.T) {
			g := NewGenerator(N)

			res1 := g.Do()
			if len(res1) != N {
				t.Errorf("wrong length(%d): %s", len(res1), res1)
			}

			res2 := g.Do()
			if res1 == res2 {
				t.Fatal("result not changed")
			}
		}
	}

	for _, n := range [5]int{6, 8, 10, 14, 20} {
		t.Run(fmt.Sprintf("N=%d", n), fn(n))
	}
}

func BenchmarkGeneratorRand(b *testing.B) {
	fn := func(N int) func(*testing.B) {
		return func(b *testing.B) {
			g := NewGenerator(8)
			for i := 0; i < b.N; i++ {
				_ = g.randDo()
			}
		}
	}

	for _, n := range [5]int{6, 8, 10, 14, 20} {
		b.Run(fmt.Sprintf("N=%d", n), fn(n))
	}
}

func BenchmarkGeneratorRand2(b *testing.B) {
	fn := func(N int) func(*testing.B) {
		return func(b *testing.B) {
			g := NewGenerator(8)
			for i := 0; i < b.N; i++ {
				_ = g.rand2Do()
			}
		}
	}

	for _, n := range [5]int{6, 8, 10, 14, 20} {
		b.Run(fmt.Sprintf("N=%d", n), fn(n))
	}
}

func BenchmarkGeneratorCrypto(b *testing.B) {
	fn := func(N int) func(*testing.B) {
		return func(b *testing.B) {
			g := NewGenerator(8)
			for i := 0; i < b.N; i++ {
				_ = g.cryptoDo()
			}
		}
	}

	for _, n := range [5]int{6, 8, 10, 14, 20} {
		b.Run(fmt.Sprintf("N=%d", n), fn(n))
	}
}
