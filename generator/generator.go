package generator

import (
	crand "crypto/rand"
	"math/big"
	mrand "math/rand"
	"time"

	"git.autistici.org/blallo/shottino"
)

const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-"

var (
	max    int
	idxMax *big.Int
)

func init() {
	max = len(chars)
	idxMax = big.NewInt(int64(max))
	mrand.Seed(time.Now().UnixNano())
}

type Generator struct {
	length int
}

func (g *Generator) Charset() string {
	return "a-zA-Z0-9_-"
}

func (g *Generator) Do() string {
	return g.rand2Do()
}

func (g *Generator) randDo() string {
	result := make([]byte, g.length)
	for i := 0; i < g.length; i++ {
		result[i] = chars[mrand.Intn(max)]
	}

	return string(result)
}

func (g *Generator) cryptoDo() string {
	result := make([]byte, g.length)
	for i := 0; i < g.length; i++ {
		idx, err := crand.Int(crand.Reader, idxMax)
		if err != nil {
			continue
		}
		result[i] = chars[idx.Int64()]
	}

	return string(result)
}

func (g *Generator) rand2Do() string {
	result := make([]byte, g.length)

	j := 0
mainLoop:
	for {
		r := mrand.Int63()
		for i := 0; i < 10; i++ {
			if j >= g.length {
				break mainLoop
			}
			idx := (r & int64(63<<(6*i))) >> (6 * i)
			result[j] = chars[idx]
			j += 1
		}
	}

	return string(result)
}

func NewGenerator(length int) *Generator {
	return &Generator{
		length: length,
	}
}

var _ shottino.Generator = &Generator{}
