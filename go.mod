module git.autistici.org/blallo/shottino

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/mattn/go-sqlite3 v1.14.12
	github.com/patrickmn/go-cache v2.1.0+incompatible
)

require (
	git.sr.ht/~blallo/logz/interface v0.0.0-20220321203905-b2473bc44a3d // indirect
	git.sr.ht/~blallo/logz/jsonlogger v0.0.0-20220321212101-81677a2efc87 // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/text v0.3.7 // indirect
)
